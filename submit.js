const form = document.getElementById('myForm');
const url = form.getAttribute('action');

function addText(text) {
    const newElement = document.createElement('span');
    newElement.innerText = text;
    form.appendChild(newElement);
}

function submitHandler (event) {
    event.preventDefault();

    const data = new FormData(form);
    data.append('newField', 'customValue');
    data.delete('hiddenInput');

    fetch(url, {
        body: data,
        method: 'POST',
    });

    addText('Форма отправлена!');
}

form.onsubmit = submitHandler;